require 'capybara'
require 'capybara/cucumber'
require 'capybara/dsl'
require 'site_prism'
require 'pry'
require 'rspec'

include RSpec::Matchers
include Capybara::DSL
 

MASS = (YAML.load_file('./features/fixtures/mass.yml'))

Capybara.register_driver :selenium_chrome_headless do |app|
  Capybara::Selenium::Driver.load_selenium
  browser_options = ::Selenium::WebDriver::Chrome::Options.new.tap do |opts|
    opts.args << "--headless"
    #opts.args << "user-agent="
    opts.args << "--disable-gpu" if Gem.win_platform?
    opts.args << "--no-sandbox"
    opts.args << "--disable-site-isolation-trials"
    opts.args << "window-size=1280,1024"
    opts.args << "disable-dev-shm-usage"
  end
  Capybara::Selenium::Driver.new(app, browser: :chrome, options: browser_options)
end

Capybara.configure do |config|

###############################################################
    # cucumber -p chrome - @profile_loja for igual "chrome" então execute no modo grafico
    # padrão = -p ci para execução headless
    if ENV['env'] == "chrome"  
      # Executar no chrome driver padrão
      config.default_driver = :selenium_chrome
    else
      # Executar no CI/CD driver headless
      config.default_driver = :selenium_chrome_headless  
    end
  config.default_max_wait_time = 15
  config.app_host = "https://www.lojavirtual.pro.br"
  Capybara.page.driver.browser.manage.window.maximize if ENV['env'] == "chrome"
end
