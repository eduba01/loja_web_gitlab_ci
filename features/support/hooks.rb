 
Before do
    #page.current_window.resize_to(1366, 768)
    @home_page = HomePage.new
    @produto_page = ProdutoPage.new
    @carrinho_page = CarrinhoPage.new
end

After do |step|
    nome_step = step.name.gsub(/[^A-Za-z0-9 ]/, '')
    nome_step = nome_step.gsub(' ', '_').downcase!
    #nome_step += '-' + Time.now.strftime('%d%m%Y-%H%M%S').to_s
    screenshot = "log/screenshots/#{nome_step}.png"
    page.save_screenshot(screenshot)
    shot = Base64.encode64(File.open(screenshot, "rb").read)
    embed(shot, "image/png", "Evidência")
end
 
After do |scenario|
    if page.driver.browser.window_handles.count > 1
      puts page.driver.browser.window_handles.count
      page.driver.browser.close
      last_handle = page.driver.browser.window_handles.last
      page.driver.browser.switch_to.window(last_handle)
    end
          url_atual = (URI.parse(current_url)).to_s
          unless url_atual.include?("lojavirtual")
            # Fazer logout
            page.reset_session!  # faz logout - Limpa a sessão do usuário
          end
  end