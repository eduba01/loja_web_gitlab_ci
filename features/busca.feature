#language: pt
#utf-8

@busca
Funcionalidade: Busca
  Eu como usuario
  Quero realizar buscas no site
  Para validar o resultado
 
 
@positivo
Cenário: Validar busca por um produto valido
  Dado que eu esteja na home page da loja
  Quando realizar uma busca na home page por "produto_valido"
  Então produto "Sony PlayStation 3" deve ser apresentado no resultado da busca
 
@negativo
Cenário: Validar busca por um produto inválido
  Dado que eu esteja na home page da loja
  Quando realizar uma busca na home page por "produto_invalido"
  Então devo visualizar mensagem de erro "Resultado da pesquisa para \"0000000\""
 
@negativo
Esquema do Cenário:  Validar busca com erro
  Dado que eu esteja na home page da loja
  Quando realizar uma busca na home page por <produto>
  Então devo visualizar mensagem de erro <mensagem>
  
  Exemplos:
  | produto  | mensagem                                 |
  | "vzcvzx" | "Resultado da pesquisa para \"vzcvzx\""  |
  | "()@"    | "Resultado da pesquisa para \"()@\""     |
  | ""       | ""                                       |
