 
Dado("que eu esteja na home page da loja") do
  @home_page.load
end
  
  Quando(/^realizar uma busca na home page por "([^"]*)"$/) do |produto| 
    @home_page.busca_home(produto)
  end
 
Então("produto {string} deve ser apresentado no resultado da busca") do |produto|
  expect(page).to have_content produto
  # Validação utilizando método
  @home_page.validar_titulo_pagina(produto)
end

Então("devo visualizar mensagem de erro {string}") do |mensagem|
  expect(page).to have_content mensagem
end
 