Quando("adicionar um produto no carrinho") do
    @produto_page.adicionar_produto
 end
 
Então("devo ter o produto buscado no carrinho") do
    @carrinho_page.validar_prod_no_carrinho
  end
 
Quando("fazer login no pagina {string} com {string}") do |pagina, status|
  @carrinho_page.fazer_login(pagina,status)
end

Então("login deve ser realizado com sucesso") do
  expect(page).to have_content (MASS['USUARIOS']['usuario_valido']['nome'])
  expect(page).to have_content (MASS['USUARIOS']['usuario_valido']['email'])
end

Quando("fechar pedido com {string}") do |pagamento|
  @carrinho_page.selecinar_pagamento(pagamento)
end

Então("mensagem de erro deve ser apresentada") do
  expect(page).to have_content "não foi localizado em nossa loja"
end

Então("pedido com {string} deve ser realizado com sucesso") do |pagamento|
  # Exibindo Recaptcha ao finalizar o pagamento
  expect(page).to have_content ""
end

  