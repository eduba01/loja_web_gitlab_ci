#language: pt
#utf-8

@carrinho
Funcionalidade: Carrinho
  Eu como usuario
  Quero validar o carrinho da loja
  Para validar produto no carrinho
 
@negativo
Cenário: Validar login no carrinho com erro
  Dado que eu esteja na home page da loja
  Quando realizar uma busca na home page por "produto_valido"
  E adicionar um produto no carrinho
  Então devo ter o produto buscado no carrinho
  Quando fazer login no pagina "carrinho" com "erro"
  Então mensagem de erro deve ser apresentada
  
@positivo
Cenário: Validar produto no carrinho
  Dado que eu esteja na home page da loja
  Quando realizar uma busca na home page por "produto_valido"
  E adicionar um produto no carrinho
  Então devo ter o produto buscado no carrinho
  
@positivo
Cenário: Validar login no carrinho com sucesso
  Dado que eu esteja na home page da loja
  Quando realizar uma busca na home page por "produto_valido"
  E adicionar um produto no carrinho
  Então devo ter o produto buscado no carrinho
  Quando fazer login no pagina "carrinho" com "sucesso"
  Então login deve ser realizado com sucesso

@positivo
Cenário: Validar pedido com boleto com sucesso
  Dado que eu esteja na home page da loja
  Quando realizar uma busca na home page por "produto_valido"
  E adicionar um produto no carrinho
  Então devo ter o produto buscado no carrinho
  Quando fazer login no pagina "carrinho" com "sucesso"
  Então login deve ser realizado com sucesso
  Quando fechar pedido com "boleto"
  Então pedido com "boleto" deve ser realizado com sucesso

  

