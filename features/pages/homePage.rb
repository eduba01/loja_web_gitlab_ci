class HomePage < SitePrism::Page
  include Capybara::DSL

    # Objetos da Home Page 
    element :lbl_busca, :css, '#searchBox'
    element :btn_busca, :css, '.bx-search'
    element :txt_titulo_pagina, :css, 'div.col-md-9.col-sm-8.col-sm-12.col-lg-10'
 
    def load
      visit ""
    end
 
    def busca_home(produto)
      #wait_until_lbl_busca_visible
        if produto == "produto_valido"
           produto = MASS['PRODUTOS'][produto]['nome']
        elsif
           produto == "produto_invalido"
           produto = MASS['PRODUTOS'][produto]['nome']
        end

        # Elementos dentro do escopo de busca
        within(:xpath, "//*[@id='topo']/div[1]/div/div[2]/div/div[1]/form") do
          lbl_busca.set produto 
          btn_busca.click
        end
    end
 
    def validar_titulo_pagina(titulo) 
      txt_titulo_pagina.text.should include(titulo)
    end
  
end
  
