class CarrinhoPage < SitePrism::Page
  include Capybara::DSL

    # Objetos do Carrinho 
    element :produto, :css, 'div.container-fluid.miolo.largura-loja > div > div.col-md-9.col-sm-8.col-sm-12.col-lg-10'
    element :preco, :css, 'div.container-fluid.miolo.largura-loja > div > div.col-md-9.col-sm-8.col-sm-12.col-lg-10'
    element :lbl_cep, :css, 'input.form-control.input-loja'
    element :btn_calcular, :css, '#Submit1' 
    element :btn_continuar, :css, 'div.container-fluid.miolo.largura-loja > div > div.col-md-9.col-sm-8.col-sm-12.col-lg-10 > div > form:nth-child(2) > div > div:nth-child(3) > button'  
    # Objetos da tela de login
    element :lbl_email, :css, 'body > div.container-fluid.miolo.largura-loja > div > div:nth-child(2) > div > div:nth-child(1) > div > div.panel-body > form > div:nth-child(1) > input'
    element :lbl_senha, :css, 'body > div.container-fluid.miolo.largura-loja > div > div:nth-child(2) > div > div:nth-child(1) > div > div.panel-body > form > div:nth-child(2) > input.form-control.input-sm'
    element :btn_prosseguir, :css, 'body > div.container-fluid.miolo.largura-loja > div > div:nth-child(2) > div > div:nth-child(1) > div > div.panel-body > form > div:nth-child(3) > button' 
    # Objetos da tela de pagamento
    element :pgto_boleto, :css, '#accordion > div:nth-child(2) > div.panel-heading > h4 > a:nth-child(2)'
    element :btn_finalizar, :css, '#collapse14 > div > input.btn.btn-success.btn-block.g-recaptcha'

    def validar_prod_no_carrinho
      # Valida se o nome do produto é igual ao da massa
      #binding.pry
      @produto = MASS['PRODUTOS']['produto_valido']['nome']
      @produto = @produto.upcase
      # Clicar em continuar quanto tiver o opção de garantia estendida
      #continuar = expect(page).to have_css("#btn-continue")
        # if continuar == true
        #   btn_continuar.click
        #   produto.text.upcase.should include(@produto) 
        #   # Valida se o preço do produto é igual ao da massa
        #   @preco = MASS['PRODUTOS']['produto_valido']['preco']
        #   preco.text.should include(MASS['PRODUTOS']['produto_valido']['preco'])  
        # else
          produto.text.upcase.should include(@produto) 
          # Valida se o preço do produto apresentado é igual ao da massa
          @preco = MASS['PRODUTOS']['produto_valido']['preco']
          preco.text.should include(MASS['PRODUTOS']['produto_valido']['preco'])  
          lbl_cep.set "05037001"
          btn_calcular.click
          click_button 'Clique aqui para iniciar o pagamento'
          sleep 2
          wait_until_lbl_email_visible
        #end
    end

    def fazer_login(pagina,status)
        case status
        when "sucesso"
          lbl_email.set (MASS['USUARIOS']['usuario_valido']['email'])
          lbl_senha.set (MASS['USUARIOS']['usuario_valido']['senha'])
        when "erro"
          lbl_email.set (MASS['USUARIOS']['usuario_invalido']['email'])
          lbl_senha.set (MASS['USUARIOS']['usuario_invalido']['senha'])
        end
      btn_prosseguir.click
    end
    
    def selecinar_pagamento(pagamento)
      #binding.pry
      pgto_boleto.click
      wait_until_btn_finalizar_visible
      btn_finalizar.click
    end
end
  
