class ProdutoPage < SitePrism::Page
  include Capybara::DSL

    # Objetos do produto 
    element :lnk_produto1, :css, 'div.box_imagem_vitrine'
    element :btn_tamanho, :css, 'span:nth-child(4) > label'
    element :btn_cor, :css, 'span.radio-cor12'
    element :btn_qtd, :css, 'span.input-group-btn.input-group-append'
    element :btn_comprar, :css, 'button.btn.btn-success.bot-comprar-pp'

    def adicionar_produto
      wait_until_lnk_produto1_visible
      sleep 1
      lnk_produto1.click
      sleep 1
      btn_comprar.click
      #click_button 'Comprar'
    end
 
end
  
